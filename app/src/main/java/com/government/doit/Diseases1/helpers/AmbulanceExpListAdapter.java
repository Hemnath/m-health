package com.government.doit.Diseases1.helpers;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.government.doit.Diseases1.MainActivity;
import com.government.doit.Diseases1.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by ik890 on 10/5/2016.
 */

public class AmbulanceExpListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private HashMap<DistrictItem, List<AmulanceItem>> AmulanceItems = new HashMap<DistrictItem, List<AmulanceItem>>();
    private HashMap<DistrictItem, List<AmulanceItem>> AmulanceSearchItems = new HashMap<DistrictItem, List<AmulanceItem>>();
    private List<DistrictItem> groupItems = new ArrayList<DistrictItem>();
    private List<DistrictItem> groupSearch = new ArrayList<DistrictItem>();

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String strLangType = "LangType";
    private String langType;
    String strPhone, strMobile;

    public AmbulanceExpListAdapter(Context context) {
        this.mContext = context;
        sharedpreferences = mContext.getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        langType = sharedpreferences.getString(strLangType, null);
    }

    public void setExpandableList(String district_name_en, String district_name_np, int district_id, int ambulance_id, String organization_name_en, String organization_name_np, String organization_address_np, String organization_address_en, String phone_no_en, String phone_no_np, String vehicle_no_en, String vehicle_no_np, String mobile_no_en, String mobile_no_np, String id) {
        boolean isAdded = false;
        for (int i = 0; i < groupItems.size(); i++) {
            DistrictItem item = groupItems.get(i);

            if (item.district_id == district_id) {

                List<AmulanceItem> childItemList = AmulanceItems.get(groupItems.get(i));
                List<AmulanceItem> childItemList2 = AmulanceSearchItems.get(groupSearch.get(i));

                AmulanceItem AmulanceItem = new AmulanceItem(organization_name_en, organization_name_np, organization_address_np, organization_address_en, phone_no_en, phone_no_np, vehicle_no_en, vehicle_no_np, mobile_no_en, mobile_no_np, id);
                childItemList.add(AmulanceItem);
                AmulanceItems.put(groupItems.get(i), childItemList);
                AmulanceSearchItems.put(groupSearch.get(i), childItemList2);
                isAdded = true;
            }
        }

        if (!isAdded) {

            DistrictItem dItem = new DistrictItem(district_name_en, district_name_np, district_id, ambulance_id);
            groupItems.add(dItem);
            groupSearch.add(dItem);
            List<AmulanceItem> childItemList = new ArrayList<AmulanceItem>();

            AmulanceItem AmulanceItem = new AmulanceItem(organization_name_en, organization_name_np, organization_address_np, organization_address_en, phone_no_en, phone_no_np, vehicle_no_en, vehicle_no_np, mobile_no_en, mobile_no_np, id);
            childItemList.add(AmulanceItem);
            AmulanceItems.put(dItem, childItemList);
            AmulanceSearchItems.put(dItem, childItemList);
        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        clearItem();
        if (charText.length() == 0) {
            addAll();
        } else {
            for (DistrictItem wp : groupSearch) {
                if (wp.district_name_en.toLowerCase(Locale.getDefault()).contains(charText) || wp.district_name_np.contains(charText)) {
                    groupItems.add(wp);
                } else {
                    List<AmulanceItem> childItemList = AmulanceItems.get(wp);
                    List<AmulanceItem> childItemList2 = AmulanceSearchItems.get(wp);
                    List<AmulanceItem> childNewItemList = new ArrayList<AmulanceItem>();
                    for (AmulanceItem ci : childItemList2) {
                        if (ci.organization_address_en.toLowerCase(Locale.getDefault()).contains(charText) || ci.organization_address_np.contains(charText)) {
                            childNewItemList.add(ci);
                        }
                    }
                    if (childNewItemList.size() > 0) {
                        childItemList.clear();
                        childItemList.addAll(childNewItemList);
                        groupItems.add(wp);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public boolean clearItem() {
        groupItems.clear();
        return true;
    }

    public void addAll() {
        groupItems.clear();
        for (DistrictItem wp : groupSearch) {
            List<AmulanceItem> childItemList = AmulanceItems.get(wp);
            List<AmulanceItem> childItemList2 = AmulanceSearchItems.get(wp);
            List<AmulanceItem> childNewItemList = new ArrayList<AmulanceItem>();
            for (AmulanceItem ci : childItemList2) {
                childNewItemList.add(ci);
            }
            if (childNewItemList.size() > 0) {
                childItemList.clear();
                childItemList.addAll(childNewItemList);
            }
        }
        groupItems.addAll(groupSearch);
    }

    @Override
    public int getGroupCount() {
        return groupItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return AmulanceItems.get(groupItems.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.groupItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.AmulanceItems.get(this.groupItems.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView lblListHeader;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.hospitals_list_group, parent, false);
            v.setTag(R.id.lblListDistrict, v.findViewById(R.id.lblListDistrict));
        }
        lblListHeader = (TextView) v.getTag(R.id.lblListDistrict);

        DistrictItem item = (DistrictItem) getGroup(groupPosition);
        if (langType.equalsIgnoreCase("1"))
            lblListHeader.setText(item.district_name_np);
        else if (langType.equalsIgnoreCase("2"))
            lblListHeader.setText(item.district_name_en);


        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView hospital_name, address;
        Button phoneno, mobileno;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.ambulances_list_child, parent, false);
            v.setTag(R.id.hospital_name, v.findViewById(R.id.hospital_name));
            v.setTag(R.id.phoneno, v.findViewById(R.id.phoneno));
            v.setTag(R.id.mobileno, v.findViewById(R.id.mobileno));
            v.setTag(R.id.address, v.findViewById(R.id.address));
        }
        hospital_name = (TextView) v.getTag(R.id.hospital_name);
        phoneno = (Button) v.getTag(R.id.phoneno);
        mobileno = (Button) v.getTag(R.id.mobileno);
        address = (TextView) v.getTag(R.id.address);

        AmulanceItem item = (AmulanceItem) getChild(groupPosition, childPosition);
        if (langType.equalsIgnoreCase("1")) {
            String name = item.organization_name_np + "  ( " + item.vehicle_no_np + " )";
            hospital_name.setText(name);
            address.setText(item.organization_address_np);
            phoneno.setText(item.phone_no_np);
            mobileno.setText(item.mobile_no_np);
        } else if (langType.equalsIgnoreCase("2")) {
            String name = item.organization_name_en + "  ( " + item.vehicle_no_en + " )";
            hospital_name.setText(name);
            address.setText(item.organization_address_en);
            phoneno.setText(item.phone_no_en);
            mobileno.setText(item.mobile_no_en);
        }
        strPhone = item.phone_no_en;
        strMobile = item.mobile_no_en;
        phoneno.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + strPhone));
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                mContext.startActivity(callIntent);
            }
        });
        mobileno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + strMobile));
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mContext.startActivity(callIntent);
            }
        });

        return v;
    }


    private class DistrictItem {
        final String district_name_en, district_name_np;
        final int district_id;
        final int ambulance_id;

        public DistrictItem(String district_name_en, String district_name_np, int district_id, int ambulance_id) {
            this.district_name_en = district_name_en;
            this.district_name_np = district_name_np;
            this.district_id = district_id;
            this.ambulance_id = ambulance_id;
        }
    }

    private class AmulanceItem {
        final String organization_name_en;
        final String organization_name_np;
        final String organization_address_np;
        final String organization_address_en;
        final String phone_no_en;
        final String phone_no_np;
        final String vehicle_no_en;
        final String vehicle_no_np;
        final String mobile_no_en;
        final String mobile_no_np;
        final String Id;

        public AmulanceItem(String organization_name_en, String organization_name_np, String organization_address_np, String organization_address_en, String phone_no_en, String phone_no_np, String vehicle_no_en, String vehicle_no_np, String mobile_no_en, String mobile_no_np, String id) {
            this.organization_name_en = organization_name_en;
            this.organization_name_np = organization_name_np;
            this.organization_address_np = organization_address_np;
            this.organization_address_en = organization_address_en;
            this.phone_no_en = phone_no_en;
            this.phone_no_np = phone_no_np;
            this.vehicle_no_en = vehicle_no_en;
            this.vehicle_no_np = vehicle_no_np;
            this.mobile_no_en = mobile_no_en;
            this.mobile_no_np = mobile_no_np;
            Id = id;
        }


    }

}