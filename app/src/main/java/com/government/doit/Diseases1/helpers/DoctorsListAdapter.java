package com.government.doit.Diseases1.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.government.doit.Diseases1.MainActivity;
import com.government.doit.Diseases1.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DoctorsListAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private List<Item> searchitems = new ArrayList<Item>();
    private LayoutInflater inflater;

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String strLangType = "LangType";
    private String langType;

    public DoctorsListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        sharedpreferences = mContext.getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        langType = sharedpreferences.getString(strLangType, null);
    }

    public void addItem(String doctor_name_en, String doctor_name_np, String doctor_address_en, String doctor_address_np, String specialization_en, String specialization_np, String id) {
        items.add(new Item(doctor_name_en, doctor_name_np, doctor_address_en, doctor_address_np, specialization_en, specialization_np, id));
        searchitems.add(new Item(doctor_name_en, doctor_name_np, doctor_address_en, doctor_address_np, specialization_en, specialization_np, id));
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        clearItem();
        if (charText.length() == 0) {
            addAll();
        } else {
            for (Item wp : searchitems) {
                if (wp.doctor_name_en.toLowerCase(Locale.getDefault()).contains(charText) || wp.doctor_name_np.contains(charText) || wp.doctor_address_en.toLowerCase(Locale.getDefault()).contains(charText) || wp.doctor_address_np.contains(charText) || wp.specialization_en.toLowerCase(Locale.getDefault()).contains(charText) || wp.specialization_np.contains(charText)) {
                    items.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public boolean clearItem() {
        items.clear();
        return true;
    }

    public void addAll() {
        items.clear();
        items.addAll(searchitems);
    }

    public String getDoctorId(int position) {
        return items.get(position).Id;
    }

    public String getDoctor_name(int position) {
        if (langType.equalsIgnoreCase("1"))
            return items.get(position).doctor_name_np;
        else if (langType.equalsIgnoreCase("2"))
            return items.get(position).doctor_name_en;
        else
            return items.get(position).doctor_name_np;
    }

    public String getDoctor_address(int position) {
        if (langType.equalsIgnoreCase("1"))
            return items.get(position).doctor_address_np;
        else if (langType.equalsIgnoreCase("2"))
            return items.get(position).doctor_address_en;
        else
            return items.get(position).doctor_address_np;
    }

    public String getSpecialization_en(int position) {
        if (langType.equalsIgnoreCase("1"))
            return items.get(position).specialization_np;
        else if (langType.equalsIgnoreCase("2"))
            return items.get(position).specialization_en;
        else
            return items.get(position).specialization_np;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView doctor_name, address, doctor_spec;

        if (v == null) {
            v = inflater.inflate(R.layout.list_item_doctors, viewGroup, false);
            v.setTag(R.id.doctor_name, v.findViewById(R.id.doctor_name));
            v.setTag(R.id.address, v.findViewById(R.id.address));
            v.setTag(R.id.doctor_spec, v.findViewById(R.id.doctor_spec));

        }

        doctor_name = (TextView) v.getTag(R.id.doctor_name);
        address = (TextView) v.getTag(R.id.address);
        doctor_spec = (TextView) v.getTag(R.id.doctor_spec);

        Item item = (Item) getItem(i);
        if (langType.equalsIgnoreCase("1")) {
            doctor_name.setText(item.doctor_name_np);
            address.setText(item.doctor_address_np);
            doctor_spec.setText(item.specialization_np);
        } else if (langType.equalsIgnoreCase("2")) {
            doctor_name.setText(item.doctor_name_en);
            address.setText(item.doctor_address_en);
            doctor_spec.setText(item.specialization_en);
        }
        return v;
    }

    private class Item {
        final String doctor_name_en;
        final String doctor_name_np;
        final String doctor_address_en;
        final String doctor_address_np;
        final String specialization_en;
        final String specialization_np;
        final String Id;

        public Item(String doctor_name_en, String doctor_name_np, String doctor_address_en, String doctor_address_np, String specialization_en, String specialization_np, String id) {
            this.doctor_name_en = doctor_name_en;
            this.doctor_name_np = doctor_name_np;
            this.doctor_address_en = doctor_address_en;
            this.doctor_address_np = doctor_address_np;
            this.specialization_en = specialization_en;
            this.specialization_np = specialization_np;
            Id = id;
        }
    }

}