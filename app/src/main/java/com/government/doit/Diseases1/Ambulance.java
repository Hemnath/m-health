package com.government.doit.Diseases1;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.government.doit.Diseases1.helpers.AlertDialogManager;
import com.government.doit.Diseases1.helpers.AmbulanceExpListAdapter;
import com.government.doit.Diseases1.helpers.ConnectionDetector;
import com.government.doit.Diseases1.helpers.HospitalExpListAdapter;
import com.government.doit.Diseases1.helpers.JSONParser;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Ambulance extends AppCompatActivity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {
    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    AmbulanceExpListAdapter ambulanceExpListAdapter;
    public static final String strLangType = "LangType";
    private String langType;
    private String URL;
    private ExpandableListView AmbulanceListview;

    ConnectionDetector cd;
    AlertDialogManager alert = new AlertDialogManager();
    private ProgressDialog pDialog;
    int lastExpandedPosition = -1;
    JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.seal);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        langType = sharedpreferences.getString(strLangType, null);


        assert langType != null;
        if (langType.equalsIgnoreCase("1"))
            getSupportActionBar().setTitle("एम्बुलेन्स");
        else if (langType.equalsIgnoreCase("2"))
            getSupportActionBar().setTitle("Ambulances");

        AmbulanceListview = (ExpandableListView) findViewById(R.id.hospitalList);
        ambulanceExpListAdapter = new AmbulanceExpListAdapter(this);
        AmbulanceListview.setAdapter(ambulanceExpListAdapter);

        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            if (langType.equalsIgnoreCase("1"))
                alert.showAlertDialog(this, "इन्टरनेट जडानमा त्रुटी !", "कृपया यो उपकरणलाई इन्टरनेट संग जोडनुहोस", false);
            else if (langType.equalsIgnoreCase("2"))
                alert.showAlertDialog(this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }

        URL = "http://202.45.144.52/hospital/api/app/ambulances_districts";
        new LoadTracks().execute(URL);

        AmbulanceListview.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    AmbulanceListview.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });
    }

    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        ambulanceExpListAdapter.filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        ambulanceExpListAdapter.filter(newText);
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_items, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, MainActivity.class)));
        searchView.setIconifiedByDefault(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    class LoadTracks extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Ambulance.this);
            pDialog.setMessage("Loading ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String json = jsonParser.makeHttpRequest(URL, "GET", params);
            return json;
        }


        protected void onPostExecute(String json) {
            pDialog.dismiss();
            try {
                JSONArray hospitalList = new JSONArray(json);
                if (hospitalList != null) {
                    for (int i = 0; i < hospitalList.length(); i++) {
                        JSONObject c = hospitalList.getJSONObject(i);
                        String id = c.getString("id");
                        String organization_name_en = c.getString("organization_name");
                        String organization_name_np = c.getString("organization_name");
                        String organization_address_np = c.getString("organization_address");
                        String organization_address_en = c.getString("organization_address");
                        String phone_no_en = c.getString("phone_no");
                        String phone_no_np = c.getString("phone_no");
                        String mobile_no_en = c.getString("mobile_no");
                        String mobile_no_np = c.getString("mobile_no");
                        String vehicle_no_en = c.getString("vehicle_no");
                        String vehicle_no_np = c.getString("vehicle_no");

                        String district_id = c.getString("district_id");
                        String district_name_en = c.getString("name_en");
                        String district_name_np = c.getString("name_np");

                        ambulanceExpListAdapter.setExpandableList(district_name_en, district_name_np, Integer.parseInt(district_id), Integer.parseInt(id), organization_name_en,  organization_name_np,  organization_address_np,  organization_address_en,  phone_no_en,  phone_no_np,  vehicle_no_en,  vehicle_no_np,  mobile_no_en,  mobile_no_np,  id);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ambulanceExpListAdapter.notifyDataSetChanged();
        }
    }
}
