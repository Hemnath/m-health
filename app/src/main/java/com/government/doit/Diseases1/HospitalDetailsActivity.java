package com.government.doit.Diseases1;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.government.doit.Diseases1.helpers.AlertDialogManager;
import com.government.doit.Diseases1.helpers.ConnectionDetector;
import com.government.doit.Diseases1.helpers.DiseaseListAdapter;
import com.government.doit.Diseases1.helpers.DoctorsListAdapter;
import com.government.doit.Diseases1.helpers.JSONParser;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HospitalDetailsActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    String Id, hospital_name;
    JSONParser jsonParser = new JSONParser();
    String mapUrl = "";
    String webUrl = "";
    String image_url = "http://202.45.144.52/hospital/";

    Button mapBtn;
    TextView txt_website, txt_phone;

    ListView diseaseList;
    DiseaseListAdapter diseaseListAdapter;
    private ListView doctorsList;
    DoctorsListAdapter doctorsListAdapter;

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String strLangType = "LangType";
    private String langType;

    TextView textView5, diseaseTxt, doctorTxt;
    ConnectionDetector cd;
    AlertDialogManager alert = new AlertDialogManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.seal);

        sharedpreferences = getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        langType = sharedpreferences.getString(strLangType, null);

        Intent i = getIntent();
        Id = i.getStringExtra("Id");
        hospital_name = i.getStringExtra("hospital_name");
        getSupportActionBar().setTitle(hospital_name);

        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            if (langType.equalsIgnoreCase("1"))
                alert.showAlertDialog(HospitalDetailsActivity.this, "इन्टरनेट जडानमा त्रुटी !", "कृपया यो उपकरणलाई इन्टरनेट संग जोडनुहोस", false);
            else if (langType.equalsIgnoreCase("2"))
                alert.showAlertDialog(HospitalDetailsActivity.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }

        txt_phone = (TextView) findViewById(R.id.phone1);
        txt_website = (TextView) findViewById(R.id.website1);
        mapBtn = (Button) findViewById(R.id.map);

        textView5 = (TextView) findViewById(R.id.textView5);
        diseaseTxt = (TextView) findViewById(R.id.diseaseTxt);
        doctorTxt = (TextView) findViewById(R.id.doctorTxt);

        assert langType != null;
        if (langType.equalsIgnoreCase("1")) {
            doctorTxt.setText("चिकित्सकहरु");
            diseaseTxt.setText("रोगहरु");
            mapBtn.setText("नक्सा हेर्नुहोस्");
        } else if (langType.equalsIgnoreCase("2")) {
            doctorTxt.setText("Doctors");
            diseaseTxt.setText("Diseases");
            textView5.setText("Be Healthy Be Happy");
        }


        diseaseList = (ListView) findViewById(R.id.DiseaseList);
        diseaseListAdapter = new DiseaseListAdapter(this);
        diseaseList.setAdapter(diseaseListAdapter);

        doctorsList = (ListView) findViewById(R.id.DoctorsList);
        doctorsListAdapter = new DoctorsListAdapter(this);
        doctorsList.setAdapter(doctorsListAdapter);


        diseaseList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        doctorsList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        doctorsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent i = new Intent(getApplicationContext(), DoctorDetaits.class);
                String doctorId = doctorsListAdapter.getDoctorId(position);
                String doctor_name = doctorsListAdapter.getDoctor_name(position);
                String doctor_address = doctorsListAdapter.getDoctor_address(position);
                String specialization = doctorsListAdapter.getSpecialization_en(position);

                i.putExtra("doctor_id", doctorId);
                i.putExtra("doctor_name", doctor_name);
                i.putExtra("doctor_address", doctor_address);
                i.putExtra("specialization", specialization);
                startActivity(i);
            }
        });
        diseaseList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent i = new Intent(getApplicationContext(), HospitalDoctor.class);
                String disease_id = diseaseListAdapter.getDiseaseId(position);
                i.putExtra("disease_id", disease_id);
                startActivity(i);
            }
        });


        txt_website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new android.content.Intent(Intent.ACTION_VIEW, Uri.parse("http://" + webUrl.replace("http://", "")));
                startActivity(intent);
            }
        });
        txt_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = txt_phone.getText().toString().trim();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + number));
                if (ActivityCompat.checkSelfPermission(HospitalDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivityForResult(callIntent, 1);
                startActivity(callIntent);
            }
        });
        mapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new android.content.Intent(Intent.ACTION_VIEW, Uri.parse(mapUrl));
                startActivity(intent);
            }
        });

        new LoadHospital().execute("");
        new DiseaseJSONParse().execute("");
        new DoctorsJSONParse().execute("");

    }

    public void setData(String name_en, String address_en, String phone_en, String website, String image, String map_url) {

        TextView txt_hospital_title1 = (TextView) findViewById(R.id.hospital_title1);
        TextView txt_address1 = (TextView) findViewById(R.id.address1);

        txt_hospital_title1.setText(name_en);
        txt_address1.setText(address_en);
        txt_phone.setText(phone_en);
        txt_website.setText(website);

        mapUrl = map_url;
        webUrl = website;
        if (!image.isEmpty() || !image.equalsIgnoreCase("") || !image.equalsIgnoreCase("")) {
            if (!image.contains("http://"))
                image_url = image_url + image;
            else
                image_url = image;

            new Retrievedata().execute("");
        }
    }


    class Retrievedata extends AsyncTask<String, Void, String> {
        Drawable dr;

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(image_url);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                dr = new BitmapDrawable(myBitmap);

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return null;
        }

        protected void onPostExecute(String json) {
            CollapsingToolbarLayout ctl = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
            ctl.setBackground(dr);
        }
    }

    class LoadHospital extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(HospitalDetailsActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String strURL = "http://202.45.144.52/hospital/api/app/hospitalsOnly/id/" + Id;
            System.out.println("url: " + strURL);
            String json = jsonParser.makeHttpRequest(strURL, "GET", params);
            android.util.Log.d("Track List JSON: ", json);
            return json;
        }


        protected void onPostExecute(String json) {
            pDialog.dismiss();
            try {
                JSONArray hospitalList = new JSONArray(json);
                if (hospitalList != null) {
                    for (int i = 0; i < hospitalList.length(); i++) {
                        JSONObject jObj = hospitalList.getJSONObject(i);
                        String name_en = jObj.getString("name_en");
                        String name_np = jObj.getString("name_np");
                        String address_en = jObj.getString("address_en");
                        String address_np = jObj.getString("address_np");
                        String phone_en = jObj.getString("phone_en");
                        String phone_np = jObj.getString("phone_np");
                        String image = jObj.getString("image");
                        String map_url = jObj.getString("map_url");
                        String website = jObj.getString("website");

                        if (langType.equalsIgnoreCase("1")) {
                            setData(name_np, address_np, phone_np, website, image, map_url);
                        } else if (langType.equalsIgnoreCase("2")) {
                            setData(name_en, address_en, phone_en, website, image, map_url);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    class DoctorsJSONParse extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String url = "http://202.45.144.52/hospital/api/app/doctors_by_hospital/id/" + Id;
            String json = jsonParser.makeHttpRequest(url, "GET", params);

            return json;
        }

        protected void onPostExecute(String json) {
            try {
                JSONArray diseases = new JSONArray(json);
                if (diseases != null) {
                    for (int i = 0; i < diseases.length(); i++) {
                        JSONObject c = diseases.getJSONObject(i);
                        JSONObject jsonObject = diseases.getJSONObject(i);
                        String id = jsonObject.optString("id").toString();
                        String doctor_name_en = jsonObject.optString("doctor_name_en").toString();
                        String doctor_name_np = jsonObject.optString("doctor_name_np").toString();
                        String doctor_address_en = jsonObject.optString("doctor_address_en").toString();
                        String doctor_address_np = jsonObject.optString("doctor_address_np").toString();
                        String specialization_en = jsonObject.optString("specialization_en").toString();
                        String specialization_np = jsonObject.optString("specialization_np").toString();

                        doctorsListAdapter.addItem(doctor_name_en, doctor_name_np, doctor_address_en, doctor_address_np, specialization_en, specialization_np, id);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            doctorsListAdapter.notifyDataSetChanged();
        }
    }

    class DiseaseJSONParse extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String url = "http://202.45.144.52/hospital/api/app/diseases_by_hospital/id/" + Id;
            String json = jsonParser.makeHttpRequest(url, "GET", params);

            return json;
        }

        protected void onPostExecute(String json) {
            try {
                JSONArray diseases = new JSONArray(json);
                if (diseases != null) {
                    for (int i = 0; i < diseases.length(); i++) {
                        JSONObject c = diseases.getJSONObject(i);
                        JSONObject jsonObject = diseases.getJSONObject(i);
                        String disease_id = jsonObject.optString("disease_id").toString();
                        String disease_en = jsonObject.optString("disease_en");
                        String disease_np = jsonObject.optString("disease_np").toString();
                        diseaseListAdapter.addItem(disease_id, disease_en, disease_np);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            diseaseListAdapter.notifyDataSetChanged();
        }
    }
}