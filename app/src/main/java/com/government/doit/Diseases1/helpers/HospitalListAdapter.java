package com.government.doit.Diseases1.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.government.doit.Diseases1.MainActivity;
import com.government.doit.Diseases1.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HospitalListAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private List<Item> searchitems = new ArrayList<Item>();
    private LayoutInflater inflater;

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String strLangType = "LangType";
    private String langType;

    public HospitalListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        sharedpreferences = mContext.getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        langType = sharedpreferences.getString(strLangType, null);
    }

    public void addItem(String name_en, String name_np, String address_en, String address_np, String phone_en, String phone_np, String website, String image, String map_url, String id) {
        items.add(new Item(name_en, name_np, address_en, address_np, phone_en, phone_np, website, image, map_url, id));
        searchitems.add(new Item(name_en, name_np, address_en, address_np, phone_en, phone_np, website, image, map_url, id));
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        clearItem();
        if (charText.length() == 0) {
            addAll();
        } else {
            for (Item wp : searchitems) {
                if (wp.name_en.toLowerCase(Locale.getDefault()).contains(charText) || wp.name_np.contains(charText) || wp.address_en.toLowerCase(Locale.getDefault()).contains(charText) || wp.address_np.contains(charText)) {
                    items.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public boolean clearItem() {
        items.clear();
        return true;
    }

    public void addAll() {
        items.clear();
        items.addAll(searchitems);
    }

    public String get_Id(int i) {
        return items.get(i).Id;
    }

    public String get_name(int i) {
        String name = "";
        if (langType.equalsIgnoreCase("1")) {
            name = items.get(i).name_np;
        } else if (langType.equalsIgnoreCase("2")) {
            name = items.get(i).name_en;
        }
        return name;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView hospital_name, address;

        if (v == null) {
            v = inflater.inflate(R.layout.item_hospital_list, viewGroup, false);
            v.setTag(R.id.hospital_name, v.findViewById(R.id.hospital_name));
            v.setTag(R.id.address, v.findViewById(R.id.address));

        }

        hospital_name = (TextView) v.getTag(R.id.hospital_name);
        address = (TextView) v.getTag(R.id.address);

        Item item = (Item) getItem(i);
        if (langType.equalsIgnoreCase("1")) {
            hospital_name.setText(item.name_np);
            address.setText(item.address_np);
        } else if (langType.equalsIgnoreCase("2")) {
            hospital_name.setText(item.name_en);
            address.setText(item.address_en);
        }
        return v;
    }

    private class Item {

        final String name_en;
        final String name_np;
        final String address_en;
        final String address_np;
        final String phone_en;
        final String phone_np;
        final String website;
        final String image;
        final String map_url;
        final String Id;

        public Item(String name_en, String name_np, String address_en, String address_np, String phone_en, String phone_np, String website, String image, String map_url, String id) {
            this.name_en = name_en;
            this.name_np = name_np;
            this.address_en = address_en;
            this.address_np = address_np;
            this.phone_en = phone_en;
            this.phone_np = phone_np;
            this.website = website;
            this.map_url = map_url;
            this.image = image;
            Id = id;
        }
    }

}
