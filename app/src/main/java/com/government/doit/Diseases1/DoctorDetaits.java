package com.government.doit.Diseases1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.government.doit.Diseases1.helpers.AlertDialogManager;
import com.government.doit.Diseases1.helpers.ConnectionDetector;
import com.government.doit.Diseases1.helpers.HospitalListAdapter;
import com.government.doit.Diseases1.helpers.JSONParser;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DoctorDetaits extends AppCompatActivity {

    private String doctor_id;

    HospitalListAdapter hospitalListAdapter;

    JSONParser jsonParser = new JSONParser();

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String strLangType = "LangType";
    private String langType;
    ConnectionDetector cd;
    AlertDialogManager alert = new AlertDialogManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_detaits);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.seal);

        sharedpreferences = getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        langType = sharedpreferences.getString(strLangType, null);

        Intent i = getIntent();

        doctor_id = i.getStringExtra("doctor_id");
        String doctor_name = i.getStringExtra("doctor_name");
        String doctor_address = i.getStringExtra("doctor_address");
        String specialization = i.getStringExtra("specialization");

        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            if (langType.equalsIgnoreCase("1"))
                alert.showAlertDialog(DoctorDetaits.this, "इन्टरनेट जडानमा त्रुटी !", "कृपया यो उपकरणलाई इन्टरनेट संग जोडनुहोस", false);
            else if (langType.equalsIgnoreCase("2"))
                alert.showAlertDialog(DoctorDetaits.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }

        getSupportActionBar().setTitle(doctor_name);

        TextView txtdoctor_name = (TextView) findViewById(R.id.doctor_name);
        TextView address = (TextView) findViewById(R.id.address);
        TextView doctor_spec = (TextView) findViewById(R.id.doctor_spec);


        txtdoctor_name.setText(doctor_name);
        address.setText(doctor_address);
        doctor_spec.setText(specialization);

        ListView hospitalList = (ListView) findViewById(R.id.HospitalsList);
        hospitalListAdapter = new HospitalListAdapter(DoctorDetaits.this);
        hospitalList.setAdapter(hospitalListAdapter);
        new LoadTracks().execute();

        TextView txtHospitals = (TextView) findViewById(R.id.txtHospitals);
        TextView txtSpecialization = (TextView) findViewById(R.id.txtSpecialization);
        assert langType != null;
        if (langType.equalsIgnoreCase("1")) {
            txtHospitals.setText("अस्पतालहरु");
            txtSpecialization.setText("विशीष्टीकरण : ");
        } else if (langType.equalsIgnoreCase("2")) {
            txtHospitals.setText("Hospitals");
            txtSpecialization.setText("Specialization : ");
        }

        hospitalList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        hospitalList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent i = new Intent(DoctorDetaits.this, HospitalDetailsActivity.class);
                String Id = hospitalListAdapter.get_Id(position);
                String name = hospitalListAdapter.get_name(position);
                i.putExtra("hospital_name", name);
                i.putExtra("Id", Id);
                startActivity(i);
                startActivity(i);
            }
        });

    }

    class LoadTracks extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String url = "http://202.45.144.52/hospital/api/app/hospitals_by_doctor/id/" + doctor_id;
            String json = jsonParser.makeHttpRequest(url, "GET", params);
            android.util.Log.d("Track List JSON: ", json);
            return json;
        }


        protected void onPostExecute(String json) {
            try {
                JSONArray hospitalList = new JSONArray(json);
                if (hospitalList != null) {
                    for (int i = 0; i < hospitalList.length(); i++) {
                        JSONObject c = hospitalList.getJSONObject(i);
                        String hospital_id = c.getString("hospital_id");
                        String name_en = c.getString("name_en");
                        String name_np = c.getString("name_np");
                        String address_en = c.getString("address_en");
                        String address_np = c.getString("address_np");
                        String phone_en = c.getString("phone_en");
                        String phone_np = c.getString("phone_np");
                        String map_url = c.getString("map_url");
                        String website = c.getString("website");

                        hospitalListAdapter.addItem(name_en, name_np, address_en, address_np, phone_en, phone_np, website, "", map_url, hospital_id);
                    }
                }

            } catch (org.json.JSONException e) {
                e.printStackTrace();
            }
            hospitalListAdapter.notifyDataSetChanged();
        }
    }

}