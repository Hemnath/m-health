package com.government.doit.Diseases1.helpers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ListView;

import com.government.doit.Diseases1.DoctorDetaits;
import com.government.doit.Diseases1.HospitalDetailsActivity;
import com.government.doit.Diseases1.HospitalDoctor;
import com.government.doit.Diseases1.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DoctorFragment extends Fragment {
    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    private String URL = "http://202.45.144.52/hospital/api/app/doctors_by_disease/id/";

    String disease_id, disease_name;
    private static final String TAG_ID = "id";

    private ListView doctorsList;
    private DoctorsListAdapter doctorsListAdapter;

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        android.view.View v = inflater.inflate(R.layout.fragment_doctor, container, false);

        HospitalDoctor activity = (HospitalDoctor) getActivity();
        disease_id = activity.getDisease();

        doctorsList = (ListView) v.findViewById(R.id.doctorsList);
        doctorsListAdapter = new DoctorsListAdapter(getContext());
        doctorsList.setAdapter(doctorsListAdapter);

        new LoadTracks().execute();

        doctorsList.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(android.widget.AdapterView<?> arg0, android.view.View view, int position, long arg3) {
                Intent i = new Intent(getContext(), DoctorDetaits.class);
                String doctorId = doctorsListAdapter.getDoctorId(position);
                String doctor_name = doctorsListAdapter.getDoctor_name(position);
                String doctor_address = doctorsListAdapter.getDoctor_address(position);
                String specialization = doctorsListAdapter.getSpecialization_en(position);

                i.putExtra("doctor_id", doctorId);
                i.putExtra("doctor_name", doctor_name);
                i.putExtra("doctor_address", doctor_address);
                i.putExtra("specialization", specialization);
                startActivity(i);
            }
        });
        return v;
    }

    public boolean onTextSubmit(String s) {
        doctorsListAdapter.filter(s);
        return false;
    }

    class LoadTracks extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Loading ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(TAG_ID, disease_id));
            String json = jsonParser.makeHttpRequest(URL, "GET", params);
            android.util.Log.d("Track List JSON: ", json);
            return json;
        }


        protected void onPostExecute(String json) {
            pDialog.dismiss();
            try {
                JSONArray diseases = new JSONArray(json);
                if (diseases != null) {
                    for (int i = 0; i < diseases.length(); i++) {
                        JSONObject c = diseases.getJSONObject(i);
                        JSONObject jsonObject = diseases.getJSONObject(i);
                        String id = jsonObject.optString("id").toString();
                        String doctor_name_en = jsonObject.optString("doctor_name_en").toString();
                        String doctor_name_np = jsonObject.optString("doctor_name_np").toString();
                        String doctor_address_en = jsonObject.optString("doctor_address_en").toString();
                        String doctor_address_np = jsonObject.optString("doctor_address_np").toString();
                        String specialization_en = jsonObject.optString("specialization_en").toString();
                        String specialization_np = jsonObject.optString("specialization_np").toString();

                        doctorsListAdapter.addItem(doctor_name_en, doctor_name_np, doctor_address_en, doctor_address_np, specialization_en, specialization_np, id);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            doctorsListAdapter.notifyDataSetChanged();
        }
    }
}