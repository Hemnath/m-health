package com.government.doit.Diseases1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class SelectOption extends AppCompatActivity {
    FrameLayout btnDiseases;
    private FrameLayout btnHospital, btnDoctors, btnAmbulance;

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String strLangType = "LangType";
    private String langType;

    TextView itemName1, itemName2, itemName3, itemName4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_option);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.seal);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        langType = sharedpreferences.getString(strLangType, null);

        itemName1 = (TextView) findViewById(R.id.itemName1);
        itemName2 = (TextView) findViewById(R.id.itemName2);
        itemName3 = (TextView) findViewById(R.id.itemName3);
        itemName4 = (TextView) findViewById(R.id.itemName4);

        if (langType.equalsIgnoreCase("1")) {
            itemName1.setText("रोग");
            itemName2.setText("अस्पताल");
            itemName3.setText("चिकित्सक");
            itemName4.setText("एम्बुलेन्स");
        } /*else if (langType.equalsIgnoreCase("2")) {
            getSupportActionBar().setTitle("Department of Information Technology");
        }*/
        btnDiseases = (FrameLayout) findViewById(R.id.btnDiseases);
        btnDiseases.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Disease.class));
            }
        });
        btnHospital = (FrameLayout) findViewById(R.id.btnHospital);
        btnHospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Hospital.class));
            }
        });
        btnDoctors = (FrameLayout) findViewById(R.id.btnDoctors);
        btnDoctors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Doctors.class));
            }
        });
        btnAmbulance = (FrameLayout) findViewById(R.id.btnAmbulance);
        btnAmbulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Ambulance.class));
            }
        });

    }
}
