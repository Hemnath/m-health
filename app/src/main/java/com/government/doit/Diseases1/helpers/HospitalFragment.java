package com.government.doit.Diseases1.helpers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.government.doit.Diseases1.HospitalDetailsActivity;
import com.government.doit.Diseases1.HospitalDoctor;
import com.government.doit.Diseases1.MainActivity;
import com.government.doit.Diseases1.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HospitalFragment extends Fragment {

    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    HospitalListAdapter hospitalListAdapter;
    private String URL = "http://202.45.144.52/hospital/api/app/districts/id/";

    String disease_id, disease_name;
    private static final String TAG_ID = "id";
    private static final String TAG_HOSPITAL = "hospitals";
    private static final String TAG_DISEASE = "disease_en";

    private ListView hospitalList;

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_hospital, container, false);

        HospitalDoctor activity = (HospitalDoctor) getActivity();
        disease_id = activity.getDisease();

        hospitalList = (ListView) v.findViewById(R.id.hospitalList);
        hospitalListAdapter = new HospitalListAdapter(getContext());
        hospitalList.setAdapter(hospitalListAdapter);
        new LoadTracks().execute();

        hospitalList.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(android.widget.AdapterView<?> arg0, android.view.View view, int position, long arg3) {
                Intent i = new Intent(getContext(), HospitalDetailsActivity.class);
                String Id = hospitalListAdapter.get_Id(position);
                String name = hospitalListAdapter.get_name(position);
                i.putExtra("hospital_name", name);
                i.putExtra("Id", Id);
                startActivity(i);
            }
        });
        return v;
    }

    public boolean onTextSubmit(String s) {
        hospitalListAdapter.filter(s);
        return false;
    }

    class LoadTracks extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Loading ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(TAG_ID, disease_id));
            String json = jsonParser.makeHttpRequest(URL, "GET", params);
            android.util.Log.d("Track List JSON: ", json);
            return json;
        }


        protected void onPostExecute(String json) {
            pDialog.dismiss();
            try {
                org.json.JSONObject jObj = new JSONObject(json);
                if (jObj != null) {
                    disease_id = jObj.getString(TAG_ID);
                    disease_name = jObj.getString(TAG_DISEASE);
                    JSONArray hospitalList = jObj.getJSONArray(TAG_HOSPITAL);
                    if (hospitalList != null) {
                        for (int i = 0; i < hospitalList.length(); i++) {
                            JSONObject c = hospitalList.getJSONObject(i);
                            String hospital_id = c.getString(TAG_ID);
                            String name_en = c.getString("name_en");
                            String name_np = c.getString("name_np");
                            String address_en = c.getString("address_en");
                            String address_np = c.getString("address_np");
                            String phone_en = c.getString("phone_en");
                            String phone_np = c.getString("phone_np");
                            String map_url = c.getString("map_url");
                            String website = c.getString("website");

                            hospitalListAdapter.addItem(name_en, name_np, address_en, address_np, phone_en, phone_np, website, "", map_url, hospital_id);
                        }
                    }
                }

            } catch (org.json.JSONException e) {
                e.printStackTrace();
            }
            hospitalListAdapter.notifyDataSetChanged();
        }
    }
}