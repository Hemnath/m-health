package com.government.doit.Diseases1;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.government.doit.Diseases1.helpers.AlertDialogManager;
import com.government.doit.Diseases1.helpers.ConnectionDetector;
import com.government.doit.Diseases1.helpers.DoctorFragment;
import com.government.doit.Diseases1.helpers.HospitalFragment;

public class HospitalDoctor extends AppCompatActivity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    HospitalFragment tab1 = new HospitalFragment();
    DoctorFragment tab2 = new DoctorFragment();
    int fragPosition = 0;

    ViewPager pager;
    MyPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence[] Titles = {"Hospital", "Doctor"};
    int Numboftabs = 2;
    TextView textView;

    String disease_id, disease_name;

    ConnectionDetector cd;
    AlertDialogManager alert = new AlertDialogManager();

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String strLangType = "LangType";
    private String langType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_doctor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.seal);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        langType = sharedpreferences.getString(strLangType, null);

        Intent i = getIntent();
        disease_id = i.getStringExtra("disease_id");
        disease_name = i.getStringExtra("disease_name");

        getSupportActionBar().setTitle(disease_name);

        if (langType.equalsIgnoreCase("1"))
            Titles = new CharSequence[]{"अस्पताल", "चिकित्सक"};
        else if (langType.equalsIgnoreCase("2"))
            Titles = new CharSequence[]{"Hospital", "Doctor"};

        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            if (langType.equalsIgnoreCase("1"))
                alert.showAlertDialog(HospitalDoctor.this, "इन्टरनेट जडानमा त्रुटी !", "कृपया यो उपकरणलाई इन्टरनेट संग जोडनुहोस", false);
            else if (langType.equalsIgnoreCase("2"))
                alert.showAlertDialog(HospitalDoctor.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }

        textView = (TextView) findViewById(R.id.textView);

        adapter = new MyPagerAdapter(getSupportFragmentManager(), Titles, Numboftabs);
        pager = (ViewPager) findViewById(R.id.vpPager);
        pager.setAdapter(adapter);

        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {

                if (position == 0) {
                    if (langType.equalsIgnoreCase("1")) {
                        textView.setText("अस्पताल रोज्नुहोस");
                    } else if (langType.equalsIgnoreCase("2")) {
                        textView.setText("Select Hospital");
                    }
                    fragPosition = 0;
                } else if (position == 1) {
                    fragPosition = 1;
                    if (langType.equalsIgnoreCase("1")) {
                        textView.setText("चिकित्सक रोज्नुहोस");
                    } else if (langType.equalsIgnoreCase("2")) {
                        textView.setText("Select Doctor");
                    }
                }
                return getResources().getColor(R.color.colorPrimary);
            }
        });

        tabs.setViewPager(pager);
    }

    public String getDisease() {
        return disease_id;
    }

    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        tab1.onTextSubmit(query);
        tab2.onTextSubmit(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        tab1.onTextSubmit(newText);
        tab2.onTextSubmit(newText);
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_items, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, MainActivity.class)));
        searchView.setIconifiedByDefault(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter

    {

        CharSequence Titles[];
        int NumbOfTabs;

        public MyPagerAdapter(FragmentManager fm, CharSequence mTitles[],
                              int mNumbOfTabsumb) {
            super(fm);

            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return tab1;
            } else if (position == 1) {
                return tab2;
            } else {
                return tab1;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }
}