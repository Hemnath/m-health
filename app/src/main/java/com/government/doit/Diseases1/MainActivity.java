package com.government.doit.Diseases1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    Button btnnepali;
    Button btnenglish;

    public static final String HOSPITALPREFERENCES = "prefs";
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor edit;
    public static final String strLangType = "LangType";

    public int currentimageindex = 0;
    ImageView slidingimage;

    private int[] IMAGE_IDS = {
            R.drawable.splash1, R.drawable.splash2, R.drawable.splash3, R.drawable.splash4

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.seal);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final Handler mHandler = new Handler();
        final Runnable mUpdateResults = new Runnable() {
            public void run() {
                AnimateandSlideShow();
            }
        };
        int delay = 1000;
        int period = 3000;

        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                mHandler.post(mUpdateResults);
            }

        }, delay, period);

        sharedpreferences = getSharedPreferences(HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        edit = sharedpreferences.edit();

        btnnepali = (Button) findViewById(R.id.nepali);
        btnenglish = (Button) findViewById(R.id.english);

        btnnepali.setOnClickListener(this);
        btnenglish.setOnClickListener(this);

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nepali:
                Intent intent = new Intent(this, SelectOption.class);
                edit.putString(strLangType, "1");
                edit.apply();
                startActivity(intent);
                break;

            case R.id.english:
                Intent intent2 = new Intent(this, SelectOption.class);
                edit.putString(strLangType, "2");
                edit.apply();
                startActivity(intent2);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_facebook) {
            Intent i = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/DepartmentofIT/?fref=ts"));
            startActivity(i);
        } else if (id == R.id.nav_twitter) {


            Intent i = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/departmentofi"));
            startActivity(i);

        } else if (id == R.id.nav_website) {


            Intent i = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://doit.gov.np/"));
            startActivity(i);

        } else if (id == R.id.nav_google) {


            Intent i = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://plus.google.com/u/0/114693812677759747745/posts"));
            startActivity(i);

        } else if (id == R.id.nav_location) {


            Intent intent3 = new Intent(this, MainActivity.class);
            startActivity(intent3);


        } else if (id == R.id.nav_calender) {


            Intent intent3 = new Intent(this, MainActivity.class);
            startActivity(intent3);

        } else if (id == R.id.nav_feedback)

        {

            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            emailIntent.setType("vnd.android.cursor.item/email");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"info@doit.gov.np"});

            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "About Hospital Finding App");
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(emailIntent, "Send mail using..."));

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;


    }


    private void AnimateandSlideShow() {
        slidingimage = (ImageView) findViewById(R.id.imageTitle);
        slidingimage.setImageResource(IMAGE_IDS[currentimageindex % IMAGE_IDS.length]);

        currentimageindex++;

        Animation rotateimage = AnimationUtils.loadAnimation(this, R.anim.custom_anim);
    }


}