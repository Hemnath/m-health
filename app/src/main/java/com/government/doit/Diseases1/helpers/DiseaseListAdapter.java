package com.government.doit.Diseases1.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.government.doit.Diseases1.MainActivity;
import com.government.doit.Diseases1.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DiseaseListAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private List<Item> searchitems = new ArrayList<Item>();
    private LayoutInflater inflater;

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String strLangType = "LangType";
    private String langType;

    public DiseaseListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        sharedpreferences = mContext.getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        langType = sharedpreferences.getString(strLangType, null);
    }

    public void addItem(String id, String disease_en, String disease_np) {
        items.add(new Item(id, disease_en, disease_np));
        searchitems.add(new Item(id, disease_en, disease_np));
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        clearItem();
        if (charText.length() == 0) {
            addAll();
        } else {
            for (Item wp : searchitems) {
                if (wp.Disease_en.toLowerCase(Locale.getDefault()).contains(charText) || wp.Disease_np.contains(charText)) {
                    items.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public boolean clearItem() {
        items.clear();
        return true;
    }

    public void addAll() {
        items.clear();
        items.addAll(searchitems);
    }

    public String getDiseaseId(int position) {
        return items.get(position).Id;
    }

    public String getDiseaseName(int position) {
        String name = "";
        if (langType.equalsIgnoreCase("1"))
            name = items.get(position).Disease_np;
        else if (langType.equalsIgnoreCase("2"))
            name = items.get(position).Disease_en;
        return name;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView disease_name;

        if (v == null) {
            v = inflater.inflate(R.layout.list_item_diseases, viewGroup, false);
            v.setTag(R.id.disease_name, v.findViewById(R.id.disease_name));

        }

        disease_name = (TextView) v.getTag(R.id.disease_name);


        Item item = (Item) getItem(i);
        assert langType != null;
        if (langType.equalsIgnoreCase("1"))
            disease_name.setText(item.Disease_np);
        else if (langType.equalsIgnoreCase("2"))
            disease_name.setText(item.Disease_en);


        return v;
    }

    private class Item {
        final String Disease_en;
        final String Disease_np;
        final String Id;

        private Item(String id, String disease_en, String disease_np) {
            Disease_en = disease_en;
            Disease_np = disease_np;
            Id = id;
        }
    }

}
