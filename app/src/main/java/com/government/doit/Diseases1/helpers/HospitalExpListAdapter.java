package com.government.doit.Diseases1.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.government.doit.Diseases1.MainActivity;
import com.government.doit.Diseases1.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by manish on 6/6/2016.
 */
public class HospitalExpListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private HashMap<DistrictItem, List<HospitalItem>> hospitalItems = new HashMap<DistrictItem, List<HospitalItem>>();
    private HashMap<DistrictItem, List<HospitalItem>> hospitalSearchItems = new HashMap<DistrictItem, List<HospitalItem>>();
    private List<DistrictItem> groupItems = new ArrayList<DistrictItem>();
    private List<DistrictItem> groupSearch = new ArrayList<DistrictItem>();

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String strLangType = "LangType";
    private String langType;

    public HospitalExpListAdapter(Context context) {
        this.mContext = context;
        sharedpreferences = mContext.getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        langType = sharedpreferences.getString(strLangType, null);
    }

    public void setExpandableList(String district_name_en, String district_name_np, int district_id, int hospital_id, String name_en, String name_np, String address_en, String address_np, String phone_en, String phone_np, String website, String image, String map_url, String id) {
        boolean isAdded = false;
        for (int i = 0; i < groupItems.size(); i++) {
            DistrictItem item = groupItems.get(i);

            if (item.district_id == district_id) {

                List<HospitalItem> childItemList = hospitalItems.get(groupItems.get(i));
                List<HospitalItem> childItemList2 = hospitalSearchItems.get(groupSearch.get(i));

                HospitalItem hospitalItem = new HospitalItem(name_en, name_np, address_en, address_np, phone_en, phone_np, website, image, map_url, id);
                childItemList.add(hospitalItem);
                hospitalItems.put(groupItems.get(i), childItemList);
                hospitalSearchItems.put(groupSearch.get(i), childItemList2);
                isAdded = true;
            }
        }

        if (!isAdded) {

            DistrictItem dItem = new DistrictItem(district_name_en, district_name_np, district_id, hospital_id);
            groupItems.add(dItem);
            groupSearch.add(dItem);
            List<HospitalItem> childItemList = new ArrayList<HospitalItem>();

            HospitalItem hospitalItem = new HospitalItem(name_en, name_np, address_en, address_np, phone_en, phone_np, website, image, map_url, id);
            childItemList.add(hospitalItem);
            hospitalItems.put(dItem, childItemList);
            hospitalSearchItems.put(dItem, childItemList);
        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        clearItem();
        if (charText.length() == 0) {
            addAll();
        } else {
            for (DistrictItem wp : groupSearch) {
                if (wp.district_name_en.toLowerCase(Locale.getDefault()).contains(charText) || wp.district_name_np.contains(charText)) {
                    groupItems.add(wp);
                } else {
                    List<HospitalItem> childItemList = hospitalItems.get(wp);
                    List<HospitalItem> childItemList2 = hospitalSearchItems.get(wp);
                    List<HospitalItem> childNewItemList = new ArrayList<HospitalItem>();
                    for (HospitalItem ci : childItemList2) {
                        if (ci.name_en.toLowerCase(Locale.getDefault()).contains(charText) || ci.name_np.contains(charText)) {
                            childNewItemList.add(ci);
                        }
                    }
                    if (childNewItemList.size() > 0) {
                        childItemList.clear();
                        childItemList.addAll(childNewItemList);
                        groupItems.add(wp);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public boolean clearItem() {
        groupItems.clear();
        return true;
    }

    public void addAll() {
        groupItems.clear();
        for (DistrictItem wp : groupSearch) {
            List<HospitalItem> childItemList = hospitalItems.get(wp);
            List<HospitalItem> childItemList2 = hospitalSearchItems.get(wp);
            List<HospitalItem> childNewItemList = new ArrayList<HospitalItem>();
            for (HospitalItem ci : childItemList2) {
                childNewItemList.add(ci);
            }
            if (childNewItemList.size() > 0) {
                childItemList.clear();
                childItemList.addAll(childNewItemList);
            }
        }
        groupItems.addAll(groupSearch);
    }

    public String hospitalId(int groupPosition, int childPosition) {
        HospitalItem item = (HospitalItem) getChild(groupPosition, childPosition);
        return item.Id;
    }

    public String hospitalName(int groupPosition, int childPosition) {
        String name = "";
        HospitalItem item = (HospitalItem) getChild(groupPosition, childPosition);
        if (langType.equalsIgnoreCase("1")) {
            name = item.name_np;
        } else if (langType.equalsIgnoreCase("2")) {
            name = item.name_en;
        }
        return name;
    }

    @Override
    public int getGroupCount() {
        return groupItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return hospitalItems.get(groupItems.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.groupItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.hospitalItems.get(this.groupItems.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView lblListHeader;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.hospitals_list_group, parent, false);
            v.setTag(R.id.lblListDistrict, v.findViewById(R.id.lblListDistrict));
        }
        lblListHeader = (TextView) v.getTag(R.id.lblListDistrict);

        DistrictItem item = (DistrictItem) getGroup(groupPosition);
        if (langType.equalsIgnoreCase("1"))
            lblListHeader.setText(item.district_name_np);
        else if (langType.equalsIgnoreCase("2"))
            lblListHeader.setText(item.district_name_en);


        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView hospital_name, address;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.hospitals_list_child, parent, false);
            v.setTag(R.id.hospital_name, v.findViewById(R.id.hospital_name));
            v.setTag(R.id.address, v.findViewById(R.id.address));
        }
        hospital_name = (TextView) v.getTag(R.id.hospital_name);
        address = (TextView) v.getTag(R.id.address);


        HospitalItem item = (HospitalItem) getChild(groupPosition, childPosition);
        if (langType.equalsIgnoreCase("1")) {
            hospital_name.setText(item.name_np);
            address.setText(item.address_np);
        } else if (langType.equalsIgnoreCase("2")) {
            hospital_name.setText(item.name_en);
            address.setText(item.address_en);
        }

        return v;
    }


    private class DistrictItem {
        final String district_name_en, district_name_np;
        final int district_id;
        final int hospital_id;

        public DistrictItem(String district_name_en, String district_name_np, int district_id, int hospital_id) {
            this.district_name_en = district_name_en;
            this.district_name_np = district_name_np;
            this.district_id = district_id;
            this.hospital_id = hospital_id;
        }
    }

    private class HospitalItem {
        final String name_en;
        final String name_np;
        final String address_en;
        final String address_np;
        final String phone_en;
        final String phone_np;
        final String website;
        final String image;
        final String map_url;
        final String Id;

        public HospitalItem(String name_en, String name_np, String address_en, String address_np, String phone_en, String phone_np, String website, String image, String map_url, String id) {
            this.name_en = name_en;
            this.name_np = name_np;
            this.address_en = address_en;
            this.address_np = address_np;
            this.phone_en = phone_en;
            this.phone_np = phone_np;
            this.website = website;
            this.map_url = map_url;
            this.image = image;
            Id = id;
        }

    }

}
