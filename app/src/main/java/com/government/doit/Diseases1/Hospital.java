package com.government.doit.Diseases1;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import com.government.doit.Diseases1.helpers.AlertDialogManager;
import com.government.doit.Diseases1.helpers.ConnectionDetector;
import com.government.doit.Diseases1.helpers.HospitalExpListAdapter;
import com.government.doit.Diseases1.helpers.JSONParser;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Hospital extends AppCompatActivity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    ConnectionDetector cd;
    AlertDialogManager alert = new AlertDialogManager();
    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    private String URL;
    private ExpandableListView hospitalListview;
    private HospitalExpListAdapter hospitalListAdapter;

    int lastExpandedPosition = -1;

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String strLangType = "LangType";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.seal);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        String langType = sharedpreferences.getString(strLangType, null);

        assert langType != null;
        if (langType.equalsIgnoreCase("1"))
            getSupportActionBar().setTitle("अस्पतालहरु");
        else if (langType.equalsIgnoreCase("2"))
            getSupportActionBar().setTitle("Hospitals");

        hospitalListview = (ExpandableListView) findViewById(R.id.hospitalList);
        hospitalListAdapter = new HospitalExpListAdapter(this);
        hospitalListview.setAdapter(hospitalListAdapter);

        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            if (langType.equalsIgnoreCase("1"))
                alert.showAlertDialog(Hospital.this, "इन्टरनेट जडानमा त्रुटी !", "कृपया यो उपकरणलाई इन्टरनेट संग जोडनुहोस", false);
            else if (langType.equalsIgnoreCase("2"))
                alert.showAlertDialog(Hospital.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }

        URL = "http://202.45.144.52/hospital/api/app/hospitalsOnly";
        new LoadTracks().execute(URL);

        hospitalListview.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    hospitalListview.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        // Listview on child click listener
        hospitalListview.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Intent i = new Intent(Hospital.this, HospitalDetailsActivity.class);
                String Id = hospitalListAdapter.hospitalId(groupPosition, childPosition);
                String hospital_name = hospitalListAdapter.hospitalName(groupPosition, childPosition);
                i.putExtra("Id", Id);
                i.putExtra("hospital_name", hospital_name);
                startActivity(i);
                return false;
            }
        });
    }

    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        hospitalListAdapter.filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        hospitalListAdapter.filter(newText);
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_items, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, MainActivity.class)));
        searchView.setIconifiedByDefault(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    class LoadTracks extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Hospital.this);
            pDialog.setMessage("Loading ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String json = jsonParser.makeHttpRequest(URL, "GET", params);
            return json;
        }


        protected void onPostExecute(String json) {
            pDialog.dismiss();
            try {
                JSONArray hospitalList = new JSONArray(json);
                if (hospitalList != null) {
                    for (int i = 0; i < hospitalList.length(); i++) {
                        JSONObject c = hospitalList.getJSONObject(i);
                        String hospital_id = c.getString("id");
                        String name_en = c.getString("name_en");
                        String name_np = c.getString("name_np");
                        String address_en = c.getString("address_en");
                        String address_np = c.getString("address_np");
                        String phone_en = c.getString("phone_en");
                        String phone_np = c.getString("phone_np");
                        String map_url = c.getString("map_url");
                        String website = c.getString("website");

                        String district_id = c.getString("district_id");
                        String district_name_en = c.getString("district_name_en");
                        String district_name_np = c.getString("district_name_np");

                        hospitalListAdapter.setExpandableList(district_name_en, district_name_np, Integer.parseInt(district_id), Integer.parseInt(hospital_id), name_en, name_np, address_en, address_np, phone_en, phone_np, website, "", map_url, hospital_id);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            hospitalListAdapter.notifyDataSetChanged();
        }
    }
}
