package com.government.doit.Diseases1;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.government.doit.Diseases1.helpers.AlertDialogManager;
import com.government.doit.Diseases1.helpers.ConnectionDetector;
import com.government.doit.Diseases1.helpers.DiseaseListAdapter;
import com.government.doit.Diseases1.helpers.JSONParser;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Disease extends AppCompatActivity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    ConnectionDetector cd;
    AlertDialogManager alert = new AlertDialogManager();

    private ProgressDialog pDialog;
    ListView diseaseList;
    DiseaseListAdapter diseaseListAdapter;
    private String URL;
    JSONParser jsonParser = new JSONParser();

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String strLangType = "LangType";
    private String langType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disease_english);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.seal);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        langType = sharedpreferences.getString(strLangType, null);

        assert langType != null;
        if (langType.equalsIgnoreCase("1"))
            getSupportActionBar().setTitle("रोगहरु");
        else if (langType.equalsIgnoreCase("2"))
            getSupportActionBar().setTitle("Diseases");

        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            if (langType.equalsIgnoreCase("1"))
                alert.showAlertDialog(Disease.this, "इन्टरनेट जडानमा त्रुटी !", "कृपया यो उपकरणलाई इन्टरनेट संग जोडनुहोस", false);
            else if (langType.equalsIgnoreCase("2"))
                alert.showAlertDialog(Disease.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }

        diseaseList = (ListView) findViewById(R.id.listDisease);
        diseaseListAdapter = new DiseaseListAdapter(this);
        diseaseList.setAdapter(diseaseListAdapter);

        URL = "http://202.45.144.52/hospital/api/app/diseases";

        new DiseaseJSONParse().execute(URL);

        diseaseList.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                Intent i = new Intent(getApplicationContext(), HospitalDoctor.class);
                String disease_id = diseaseListAdapter.getDiseaseId(position);
                String disease_name = diseaseListAdapter.getDiseaseName(position);
                i.putExtra("disease_id", disease_id);
                i.putExtra("disease_name", disease_name);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onClose() {
        diseaseListAdapter.addAll();
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        diseaseListAdapter.filter(s);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        diseaseListAdapter.filter(s);
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_items, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, MainActivity.class)));
        searchView.setIconifiedByDefault(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    class DiseaseJSONParse extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Disease.this);
            pDialog.setMessage("Listing Diseases ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String json = jsonParser.makeHttpRequest(URL, "GET", params);

            return json;
        }

        protected void onPostExecute(String json) {
            pDialog.dismiss();
            try {
                JSONArray diseases = new JSONArray(json);
                if (diseases != null) {
                    for (int i = 0; i < diseases.length(); i++) {
                        JSONObject c = diseases.getJSONObject(i);
                        JSONObject jsonObject = diseases.getJSONObject(i);
                        String id = jsonObject.optString("id").toString();
                        String disease_en = jsonObject.optString("disease_en");
                        String disease_np = jsonObject.optString("disease_np").toString();

                        diseaseListAdapter.addItem(id, disease_en, disease_np);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            diseaseListAdapter.notifyDataSetChanged();
        }

    }
}
