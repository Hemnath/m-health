package com.government.doit.Diseases1;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.government.doit.Diseases1.helpers.AlertDialogManager;
import com.government.doit.Diseases1.helpers.ConnectionDetector;
import com.government.doit.Diseases1.helpers.DiseaseListAdapter;
import com.government.doit.Diseases1.helpers.DoctorsListAdapter;
import com.government.doit.Diseases1.helpers.JSONParser;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Doctors extends AppCompatActivity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    ConnectionDetector cd;
    AlertDialogManager alert = new AlertDialogManager();
    DoctorsListAdapter doctorsListAdapter;
    private String URL;
    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String strLangType = "LangType";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctors);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.seal);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MainActivity.HOSPITALPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        String langType = sharedpreferences.getString(strLangType, null);

        assert langType != null;
        if (langType.equalsIgnoreCase("1"))
            getSupportActionBar().setTitle("चिकित्सकहरु");
        else if (langType.equalsIgnoreCase("2"))
            getSupportActionBar().setTitle("Doctors");

        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            if (langType.equalsIgnoreCase("1"))
                alert.showAlertDialog(Doctors.this, "इन्टरनेट जडानमा त्रुटी !", "कृपया यो उपकरणलाई इन्टरनेट संग जोडनुहोस", false);
            else if (langType.equalsIgnoreCase("2"))
                alert.showAlertDialog(Doctors.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }

        ListView doctorsList = (ListView) findViewById(R.id.DoctorsList);
        doctorsListAdapter = new DoctorsListAdapter(this);
        doctorsList.setAdapter(doctorsListAdapter);

        URL = "http://202.45.144.52/hospital/api/app/doctors";

        new DiseaseJSONParse().execute(URL);

        doctorsList.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                Intent i = new Intent(getApplicationContext(), DoctorDetaits.class);
                String doctorId = doctorsListAdapter.getDoctorId(position);
                String doctor_name = doctorsListAdapter.getDoctor_name(position);
                String doctor_address = doctorsListAdapter.getDoctor_address(position);
                String specialization = doctorsListAdapter.getSpecialization_en(position);

                i.putExtra("doctor_id", doctorId);
                i.putExtra("doctor_name", doctor_name);
                i.putExtra("doctor_address", doctor_address);
                i.putExtra("specialization", specialization);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        doctorsListAdapter.filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        doctorsListAdapter.filter(newText);
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_items, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, MainActivity.class)));
        searchView.setIconifiedByDefault(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    class DiseaseJSONParse extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Doctors.this);
            pDialog.setMessage("Listing Doctors...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String json = jsonParser.makeHttpRequest(URL, "GET", params);

            return json;
        }

        protected void onPostExecute(String json) {
            pDialog.dismiss();
            try {
                JSONArray diseases = new JSONArray(json);
                if (diseases != null) {
                    for (int i = 0; i < diseases.length(); i++) {
                        JSONObject c = diseases.getJSONObject(i);
                        JSONObject jsonObject = diseases.getJSONObject(i);
                        String id = jsonObject.optString("id");
                        String doctor_name_en = jsonObject.optString("doctor_name_en");
                        String doctor_name_np = jsonObject.optString("doctor_name_np");
                        String doctor_address_en = jsonObject.optString("doctor_address_en");
                        String doctor_address_np = jsonObject.optString("doctor_address_np");
                        String specialization_en = jsonObject.optString("specialization_en");
                        String specialization_np = jsonObject.optString("specialization_np");

                        doctorsListAdapter.addItem(doctor_name_en, doctor_name_np, doctor_address_en, doctor_address_np, specialization_en, specialization_np, id);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            doctorsListAdapter.notifyDataSetChanged();
        }

    }
}
